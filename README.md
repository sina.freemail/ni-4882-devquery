# NI-488.2 Devquery(VB.NET)

# Overview
 convert NI-488.2 VB6 sample program to VB.NET

## Requirement
* Visual Basic 2008 Express
* [NI-488.2 drivers ](http://www.ni.com/ja-jp/support/downloads/drivers/download.ni-488-2.html)

## Reference
* https://forums.ni.com/t5/Instrument-Control-GPIB-Serial/Using-the-old-NI-bas-files-for-accessing-a-GPIB-board-in-Visual/m-p/45002?view=by_date_ascending
