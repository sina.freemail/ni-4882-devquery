<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class Test
#Region "Windows フォーム デザイナによって生成されたコード "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'この呼び出しは、Windows フォーム デザイナで必要です。
		InitializeComponent()
	End Sub
	'Form は、コンポーネント一覧に後処理を実行するために dispose をオーバーライドします。
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Windows フォーム デザイナで必要です。
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Info As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents QuitButton As System.Windows.Forms.Button
	Public WithEvents RunRepeat As System.Windows.Forms.Button
	Public WithEvents ReadingsList As System.Windows.Forms.ListBox
	Public WithEvents Readings As System.Windows.Forms.Label
	Public WithEvents status As System.Windows.Forms.Label
	'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
	'Windows フォーム デザイナを使って変更できます。
	'コード エディタを使用して、変更しないでください。
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Test))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip
		Me.Info = New System.Windows.Forms.ToolStripMenuItem
		Me.QuitButton = New System.Windows.Forms.Button
		Me.RunRepeat = New System.Windows.Forms.Button
		Me.ReadingsList = New System.Windows.Forms.ListBox
		Me.Readings = New System.Windows.Forms.Label
		Me.status = New System.Windows.Forms.Label
		Me.MainMenu1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.BackColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Device Query"
		Me.ClientSize = New System.Drawing.Size(455, 322)
		Me.Location = New System.Drawing.Point(104, 125)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "Test"
		Me.Info.Name = "Info"
		Me.Info.Text = "Info"
		Me.Info.Checked = False
		Me.Info.Enabled = True
		Me.Info.Visible = True
		Me.QuitButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.QuitButton.BackColor = System.Drawing.SystemColors.Control
		Me.QuitButton.Text = "Quit"
		Me.QuitButton.Size = New System.Drawing.Size(67, 33)
		Me.QuitButton.Location = New System.Drawing.Point(352, 264)
		Me.QuitButton.TabIndex = 3
		Me.QuitButton.CausesValidation = True
		Me.QuitButton.Enabled = True
		Me.QuitButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.QuitButton.Cursor = System.Windows.Forms.Cursors.Default
		Me.QuitButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.QuitButton.TabStop = True
		Me.QuitButton.Name = "QuitButton"
		Me.RunRepeat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.RunRepeat.BackColor = System.Drawing.SystemColors.Control
		Me.RunRepeat.Text = "Run"
		Me.RunRepeat.Size = New System.Drawing.Size(67, 33)
		Me.RunRepeat.Location = New System.Drawing.Point(32, 264)
		Me.RunRepeat.TabIndex = 2
		Me.RunRepeat.CausesValidation = True
		Me.RunRepeat.Enabled = True
		Me.RunRepeat.ForeColor = System.Drawing.SystemColors.ControlText
		Me.RunRepeat.Cursor = System.Windows.Forms.Cursors.Default
		Me.RunRepeat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RunRepeat.TabStop = True
		Me.RunRepeat.Name = "RunRepeat"
		Me.ReadingsList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.ReadingsList.BackColor = System.Drawing.Color.Black
		Me.ReadingsList.ForeColor = System.Drawing.Color.Green
		Me.ReadingsList.Size = New System.Drawing.Size(385, 174)
		Me.ReadingsList.Location = New System.Drawing.Point(32, 80)
		Me.ReadingsList.TabIndex = 1
		Me.ReadingsList.CausesValidation = True
		Me.ReadingsList.Enabled = True
		Me.ReadingsList.IntegralHeight = True
		Me.ReadingsList.Cursor = System.Windows.Forms.Cursors.Default
		Me.ReadingsList.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.ReadingsList.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ReadingsList.Sorted = False
		Me.ReadingsList.TabStop = True
		Me.ReadingsList.Visible = True
		Me.ReadingsList.MultiColumn = False
		Me.ReadingsList.Name = "ReadingsList"
		Me.Readings.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.Readings.BackColor = System.Drawing.Color.Black
		Me.Readings.Text = "Readings"
		Me.Readings.ForeColor = System.Drawing.Color.Green
		Me.Readings.Size = New System.Drawing.Size(385, 25)
		Me.Readings.Location = New System.Drawing.Point(32, 48)
		Me.Readings.TabIndex = 4
		Me.Readings.Enabled = True
		Me.Readings.Cursor = System.Windows.Forms.Cursors.Default
		Me.Readings.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Readings.UseMnemonic = True
		Me.Readings.Visible = True
		Me.Readings.AutoSize = False
		Me.Readings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Readings.Name = "Readings"
		Me.status.BackColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.status.Text = "STOPPED"
		Me.status.ForeColor = System.Drawing.Color.Red
		Me.status.Size = New System.Drawing.Size(65, 17)
		Me.status.Location = New System.Drawing.Point(88, 88)
		Me.status.TabIndex = 0
		Me.status.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.status.Enabled = True
		Me.status.Cursor = System.Windows.Forms.Cursors.Default
		Me.status.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.status.UseMnemonic = True
		Me.status.Visible = True
		Me.status.AutoSize = False
		Me.status.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.status.Name = "status"
		Me.Controls.Add(QuitButton)
		Me.Controls.Add(RunRepeat)
		Me.Controls.Add(ReadingsList)
		Me.Controls.Add(Readings)
		Me.Controls.Add(status)
		MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me.Info})
		Me.Controls.Add(MainMenu1)
		Me.MainMenu1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class