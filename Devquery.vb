Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class Test
	Inherits System.Windows.Forms.Form
	Dim Dev As Short
	
	Private Function AddIbcnt() As String
		AddIbcnt = Chr(13) & Chr(10) & "ibcnt = 0x" & Hex(ibcnt)
	End Function
	
	Private Function AddIberr() As String
		If (ibsta And EERR) Then
			If (iberr = EDVR) Then AddIberr = Chr(13) & Chr(10) & "iberr = EDVR <DOS Error>"
			If (iberr = ECIC) Then AddIberr = Chr(13) & Chr(10) & "iberr = ECIC <Not CIC>"
			If (iberr = ENOL) Then AddIberr = Chr(13) & Chr(10) & "iberr = ENOL <No Listener>"
			If (iberr = EADR) Then AddIberr = Chr(13) & Chr(10) & "iberr = EADR <Address Error>"
			If (iberr = EARG) Then AddIberr = Chr(13) & Chr(10) & "iberr = EARG <Invalid argument>"
			If (iberr = ESAC) Then AddIberr = Chr(13) & Chr(10) & "iberr = ESAC <Not Sys Ctrlr>"
			If (iberr = EABO) Then AddIberr = Chr(13) & Chr(10) & "iberr = EABO <Op. aborted>"
			If (iberr = ENEB) Then AddIberr = Chr(13) & Chr(10) & "iberr = ENEB <No GPIB board>"
			If (iberr = EDMA) Then AddIberr = Chr(13) & Chr(10) & "iberr = EDMA <DMA error>"
			If (iberr = EOIP) Then AddIberr = Chr(13) & Chr(10) & "iberr = EOIP <Async I/O in prg>"
			If (iberr = ECAP) Then AddIberr = Chr(13) & Chr(10) & "iberr = ECAP <No capability>"
			If (iberr = EFSO) Then AddIberr = Chr(13) & Chr(10) & "iberr = EFSO <File sys. error>"
			If (iberr = EBUS) Then AddIberr = Chr(13) & Chr(10) & "iberr = EBUS <Command error>"
			If (iberr = ESTB) Then AddIberr = Chr(13) & Chr(10) & "iberr = ESTB <Status byte lost>"
			If (iberr = ESRQ) Then AddIberr = Chr(13) & Chr(10) & "iberr = ESRQ <SRQ stuck high>"
			If (iberr = ETAB) Then AddIberr = Chr(13) & Chr(10) & "iberr = ETAB <Table overflow>"
			If (iberr = ELCK) Then AddIberr = Chr(13) & Chr(10) & "iberr = ELCK <Interface is locked>"
            'If (iberr = EARM) Then AddIberr = Chr(13) & Chr(10) & "iberr = EARM <ibnotify callback failed to rearm>"
            'If (iberr = EHDL) Then AddIberr = Chr(13) & Chr(10) & "iberr = EHDL <Input handle is invalid>"
            'If (iberr = EWIP) Then AddIberr = Chr(13) & Chr(10) & "iberr = EWIP <Wait in progress on specified input handle>"
            'If (iberr = EPWR) Then AddIberr = Chr(13) & Chr(10) & "iberr = EPWR <The interface lost power>"
		Else
			AddIberr = Chr(13) & Chr(10) & "iberr = " & Str(iberr)
		End If
	End Function
	
	Private Function AddIbsta() As String
		Dim sta As String
		
		sta = Chr(13) & Chr(10) & "ibsta = &H" & Hex(ibsta) & " <"
		If (ibsta And EERR) Then sta = sta & " ERR"
		If (ibsta And TIMO) Then sta = sta & " TIMO"
		If (ibsta And EEND) Then sta = sta & " END"
		If (ibsta And SRQI) Then sta = sta & " SRQI"
		If (ibsta And RQS) Then sta = sta & " RQS"
		If (ibsta And CMPL) Then sta = sta & " CMPL"
		If (ibsta And LOK) Then sta = sta & " LOK"
		If (ibsta And RREM) Then sta = sta & " REM"
		If (ibsta And CIC) Then sta = sta & " CIC"
		If (ibsta And AATN) Then sta = sta & " ATN"
		If (ibsta And TACS) Then sta = sta & " TACS"
		If (ibsta And LACS) Then sta = sta & " LACS"
		If (ibsta And DTAS) Then sta = sta & " DTAS"
		If (ibsta And DCAS) Then sta = sta & " DCAS"
		sta = sta & ">"
		AddIbsta = sta
	End Function
	
	'
	'   Clear the list of readings in the test window
	'
	Private Sub ClearReadingsList()
		Dim i As Short
		If ReadingsList.Items.Count > 0 Then
			For i = 0 To ReadingsList.Items.Count - 1
				ReadingsList.Items.RemoveAt(0)
			Next i
		End If
		ReadingsList.Refresh()
	End Sub
	
	Private Sub GpibErr(ByRef msg As String)
		msg = msg & AddIbsta() & AddIberr() & AddIbcnt() & Chr(13) & Chr(13) & "I'm quitting!"
		MsgBox(msg, MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation, "Error")
		
		'  Take the device offline.
		
		ilonl(Dev, 0)
		
		End
	End Sub
	
	'
	'   Initalize the form controls.
	'
	Private Sub Test_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		GPIBglobalsRegistered = 0
		
		' Clear the List Box.
		
		Call ClearReadingsList()
		
	End Sub
	
	'
	'   Information about Devquery.
	'
	Public Sub Info_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Info.Click
		Dim msg As String
		msg = "This form queries a device using the '*IDN?' command to read back the identification code."
		MsgBox(msg, MsgBoxStyle.Information)
	End Sub
	
	Private Sub QuitButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles QuitButton.Click
		End
	End Sub
	
	Private Sub RunRepeat_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RunRepeat.Click
		Call Run()
	End Sub
	
	Private Sub Run()
		Dim rdbuf As String
		Dim wrtbuf As String
		
		Dim DisplayStr As String
		
		'   Disable QUIT button during run.
		
		QuitButton.Enabled = 0
		
		' ========================================================================
		'
		' INITIALIZATION SECTION
		'
		' ========================================================================
		
		'  Assign a unique identifier to the device and store in the variable
		'  Dev.  If the ERR bit is set in ibsta, call GpibErr with an error
		'  message. Otherwise, the device handle, Dev, is returned and is used in
		'  all subsequent calls to the device.
		
		Const BDINDEX As Short = 0 ' Board Index
		Const PRIMARY_ADDR_OF_DEV As Short = 1 ' Primary address of device
		Const NO_SECONDARY_ADDR As Short = 0 ' Secondary address of device
		Const TIMEOUT As Short = T10s ' Timeout value = 10 seconds
		Const EOTMODE As Short = 1 ' Enable the END message
		Const EOSMODE As Short = 0 ' Disable the EOS mode
		
		Dev = ildev(BDINDEX, PRIMARY_ADDR_OF_DEV, NO_SECONDARY_ADDR, TIMEOUT, EOTMODE, EOSMODE)
		If (ibsta And EERR) Then
			GpibErr(("Error opening device."))
		End If
		
		'  Clear the internal or device functions of the device.  If the error bit
		'  EERR is set in ibsta, call GpibErr with an error message.
		
		ilclr(Dev)
		If (ibsta And EERR) Then
			GpibErr(("Error clearing device."))
		End If
		
		' ========================================================================
		'
		'  MAIN BODY SECTION
		'
		'  In this application, the Main Body communicates with the instrument
		'  by writing a command to it and reading its response. This would be
		'  the right place to put other instrument communication.
		'
		' ========================================================================
		
		'  This application queries a device for its identification code by
		'  issuing the "*IDN?" command. Many instruments respond to this command
		'  with an identification string. Note, 488.2 compliant devices are
		'  required to respond to this command.  If the error bit EERR is set in
		'  ibsta, call GpibErr with an error message.
		
		wrtbuf = "*IDN?"
		ilwrt(Dev, wrtbuf, Len(wrtbuf))
		If (ibsta And EERR) Then
			GpibErr(("Error writing to device."))
		End If
		
		'  Read the identification code by calling ilrd. If the ERR bit is set in
		'  ibsta, call GpibErr with an error message.
		
		rdbuf = Space(100)
		ilrd(Dev, rdbuf, Len(rdbuf))
		If (ibsta And EERR) Then
			GpibErr(("Error reading from device."))
		End If
		
		'  The device returns a Line Feed character with the identification
		'  string. You could use the LEFT$() function which returns a
		'  specified number of characters from the left side of a string to
		'  remove the Line Feed character. The code fragment below illustrates
		'  how to use the LEFT$() function along with the GPIB global count
		'  variable, ibcntl, to copy the contents of rdbuf$ into a new string
		'  called DisplayStr. Note, that you need one less character than the
		'  total number contained in ibcntl.
		
		DisplayStr = VB.Left(rdbuf, ibcntl - 1)
		
		'   Display the list of readings.
		
		ReadingsList.Items.Add(DisplayStr)
		
		' ========================================================================
		'
		' CLEANUP SECTION
		'
		' ========================================================================
		
		'  Take the device offline.
		
		ilonl(Dev, 0)
		If (ibsta And EERR) Then
			GpibErr(("Error putting device offline."))
		End If
		
		'   Enable user inputs.
		
		QuitButton.Enabled = 1
		
	End Sub
End Class